#! /usr/bin/env nextflow
nextflow.enable.dsl = 1
PROGRAM_NAME = workflow.manifest.name
VERSION = workflow.manifest.version
SC2_GENOME_SIZE=29903
EMPTY_JSON = "${baseDir}/data/EMPTY.json"
FINALDIR = params.skip_gcp ? params.outdir : params.finaldir
CROMWELL_CONFIG = null
if (params.help) print_usage();
if (params.version) print_version();
if (workflow.commandLine.trim().endsWith(workflow.scriptName)) print_usage();
check_input_params()

process reduce_coverage {
    tag "${sample} - ${params.coverage}x"

    publishDir "${params.outdir}/${params.run_name}/", mode: 'copy', overwrite: true, pattern: "original-reads/*"

    input:
    tuple val(sample), val(workflow), path(r1), path(r2), path(primers), path(titan_opts), path(cromwell_config) from create_input_channel()
    env SINGULARITY_CACHEDIR from params.singularity_cache

    output:
    path("original-reads/*"), optional: true
    tuple val(sample), val(workflow), path("{subsample-r1.fastq.gz,fasta/*}"), path("subsample-r2.fastq.gz"), path(primers), path(titan_opts), path(cromwell_config) into TITAN_GC

    shell:
    R2_opts = workflow == "illumina_pe" ? "in2=${r2} out2=subsample-r2.fastq.gz" : ""
    single_end = workflow == "illumina_pe" ? false : true
    total_bp = params.coverage * SC2_GENOME_SIZE
    is_illumina = workflow.startsWith('illumina') ? true : false
    """
    if [ "!{workflow}" == "fasta" ]; then
        mkdir fasta
        cp !{r1} fasta/
        touch subsample-r2.fastq.gz
    else
        if [ "!{total_bp}" == "0" ]; then
           cp !{r1} subsample-r1.fastq.gz
           cp !{r2} subsample-r2.fastq.gz
        else
            if [ "!{is_illumina}" == "true" ]; then
                # Illumina data
                reformat.sh -Xmx8g \
                    in=!{r1} out=subsample-r1.fastq.gz !{R2_opts} \
                    samplebasestarget=!{total_bp} \
                    sampleseed=!{params.sampleseed} \
                    qin=33 \
                    qout=33 \
                    overwrite=t
            else
                # ONT data
                rasusa -i !{r1} -c !{params.coverage} -g !{SC2_GENOME_SIZE} -s !{params.sampleseed} | pigz --best -p !{task.cpus} > subsample-r1.fastq.gz
            fi

            if [ "!{single_end}" == "true" ]; then
                touch subsample-r2.fastq.gz
            fi
        fi

        mkdir original-reads
        if [ "!{single_end}" == "false" ]; then
           # Paired-End Reads
           ln -s `readlink !{r1}` original-reads/!{sample}_R1.fastq.gz
           ln -s `readlink !{r2}` original-reads/!{sample}_R2.fastq.gz
        else
           # Single-End Reads
           ln -s `readlink !{r1}` original-reads/!{sample}.fastq.gz
        fi
    fi
    """  
}

process titan_gc {
    stageInMode "copy"
    tag "${sample}"

    input:
    tuple val(sample), val(workflow), path(r1), path(r2), path(primers), path(titan_opts), path(cromwell_config) from TITAN_GC
    env SINGULARITY_CACHEDIR from params.singularity_cache

    output:
    path("${sample}.tar") into MERGE_INPUTS

    shell:
    R2_opts = workflow == "illumina_pe" ? "--r2 ${r2}" : ""
    params_json = params.titan_opts ? "--params ${titan_opts}" : ""
    """
    EXCLUDE="0"
    REASON=""
    if [ "!{workflow}" != "fasta" ]; then 
        TOTAL_LINES=`gzip -cd !{r1} | wc -l`
        if [ "\${TOTAL_LINES}" == "0" ]; then
            # Empty FASTQ, exclude it
            EXCLUDE="1"
            REASON="Excluded due to empty input FASTQ(s)"
        else
            FINAL_COV=`gzip -cd !{r1} | fastq-scan -g 30000 | grep "coverage" | sed -r 's/.*:[ ]*([0-9]+).[0-9]+,/\\1/'`
            if [ \${FINAL_COV} -lt !{params.min_coverage} ]; then
                EXCLUDE="1"
                REASON="Excluded due to low input coverage (expected >= !{params.min_coverage}x, observed \${FINAL_COV}x)"
            fi
        fi
    fi

    if [ "\${EXCLUDE}" == "1" ]; then
        mkdir -p ./!{params.run_name}/excluded
        make-excluded-results.py --sample !{sample} --reason "\${REASON}" --outdir ./!{params.run_name}/excluded
    else
        # Run titan
        make-inputs.py --sample !{sample} \
                    --workflow !{workflow} \
                    --r1 !{r1} !{R2_opts} \
                    --primers !{primers} !{params_json} > inputs.json

        theiacov-gc -i inputs.json -o ./!{params.run_name} --profile !{params.profile} --config !{cromwell_config}

        # rename outputs
        rm -rf ./!{params.run_name}/results
        mkdir -p ./!{params.run_name}/results ./!{params.run_name}/cromwell
        mv ./!{params.run_name}/theiacov-results.json ./!{params.run_name}/results/!{sample}.json
        mv ./!{params.run_name}/theiacov-results.tsv ./!{params.run_name}/results/!{sample}.tsv
        mv ./!{params.run_name}/theiacov-metadata.json ./!{params.run_name}/cromwell/!{sample}-metadata.json
        mv ./!{params.run_name}/cromwell-stdout.txt ./!{params.run_name}/cromwell/!{sample}-stdout.txt
        mv ./!{params.run_name}/cromwell-stderr.txt ./!{params.run_name}/cromwell/!{sample}-stderr.txt
        gzip --best ./!{params.run_name}/nextclade/*.json
    fi

    # Tarball it all
    tar -cvf !{sample}.tar ./!{params.run_name}

    # Clean Up!
    echo "deleted copied !{r1}" > !{r1}
    echo "deleted copied !{r2}" > !{r2}
    rm -rf cromwell-executions cromwell-workflow-logs
    rm -rf ./!{params.run_name}/
    """
}

process merge_results {
    publishDir "${params.outdir}/", mode: 'copy', overwrite: true, pattern: "${params.run_name}/*"
    publishDir "${FINALDIR}/", mode: 'copy', overwrite: true, pattern: "{wphl,wsvl}-${params.run_name}.tar.gz"

    conda '/opt/titan/envs/pangolin'

    input:
    path(tarballs) from MERGE_INPUTS.collect()

    output:
    path("${params.run_name}/*")
    path("{wphl,wsvl}-${params.run_name}.tar.gz")

    shell:
    """
    ls *.tar | xargs -I {} tar -xvf {}

    # Update pangolin results
    if [ -d ./!{params.run_name}/assemblies ]; then
        if ls ./!{params.run_name}/assemblies/ | grep "medaka"; then
            ls !{params.run_name}/results/ | grep ".tsv\$" | sed 's/.tsv//' | xargs -I {} \
                pangolin ./!{params.run_name}/assemblies/{}.medaka.consensus.fasta \
                    --outdir ./!{params.run_name}/pangolin_reports/ --outfile {}.pangolin_report.csv
        elif ls ./!{params.run_name}/assemblies/ | grep "ivar"; then
            ls !{params.run_name}/results/ | grep ".tsv\$" | sed 's/.tsv//' | xargs -I {} \
                pangolin ./!{params.run_name}/assemblies/{}.ivar.consensus.fasta \
                    --outdir ./!{params.run_name}/pangolin_reports/ --outfile {}.pangolin_report.csv
        else
            ls !{params.run_name}/results/ | grep ".tsv\$" | sed 's/.tsv//' | xargs -I {} \
                pangolin ./!{params.run_name}/assemblies/{}.fasta \
                    --outdir ./!{params.run_name}/pangolin_reports/ --outfile {}.pangolin_report.csv
        fi
    fi

    # Update titan-results
    if [ -d !{params.run_name}/results/ ]; then
        mv !{params.run_name}/results/ !{params.run_name}/results-temp/
        mkdir !{params.run_name}/results/
        { pangolin --all-versions && usher --version; } | tr '\n' ';'  | cut -f -6 -d ';' | sed -E 's/UShER \\(v([0-9.]+)\\)/UShER: \\1/;s/ //g' > pango-versions.txt
        ls !{params.run_name}/results-temp/ | grep ".tsv\$" | sed 's/.tsv//' | xargs -I {} \
            update-titan-results.py !{params.run_name}/results \
                                    !{params.run_name}/results-temp/{}.json \
                                    !{params.run_name}/pangolin_reports/{}.pangolin_report.csv \
                                    pango-versions.txt
    else 
        mkdir !{params.run_name}/assemblies/
        mkdir !{params.run_name}/results/
    fi

    # Merge results
    cat !{params.run_name}/results/*.json > !{params.run_name}/theiacov-results.json
    if [ -d !{params.run_name}/excluded/ ]; then
        csvtk concat \
            --num-cpus !{task.cpus} \
            --delimiter "\t" \
            --out-delimiter "\t" \
            --out-file !{params.run_name}/theiacov-results.tsv \
            !{params.run_name}/results/*.tsv ./!{params.run_name}/excluded/*.tsv
    else 
        csvtk concat \
            --num-cpus !{task.cpus} \
            --delimiter "\t" \
            --out-delimiter "\t" \
            --out-file !{params.run_name}/theiacov-results.tsv \
            !{params.run_name}/results/*.tsv
    fi

    # Cleanup
    rm -rf !{params.run_name}/results-temp/ pango-versions.txt

    # Merge into single tarball
    GROUP_NAME="wphl"
    if groups | grep wsvl ; then
        GROUP_NAME="wsvl"
    fi
    tar -cvf - ./!{params.run_name}/ 2> \${GROUP_NAME}-!{params.run_name}.txt | pigz --best -p !{task.cpus} > \${GROUP_NAME}-!{params.run_name}.tar.gz
    """
}

workflow.onComplete {
    workDir = new File("${workflow.workDir}")
    workDirSize = toHumanString(workDir.directorySize())

    println """
    Titan-NF Execution Summary
    ---------------------------
    Command Line    : ${workflow.commandLine}
    Resumed         : ${workflow.resume}
    Completed At    : ${workflow.complete}
    Duration        : ${workflow.duration}
    Success         : ${workflow.success}
    Exit Code       : ${workflow.exitStatus}
    Error Report    : ${workflow.errorReport ?: '-'}
    Launch Dir      : ${workflow.launchDir}
    Working Dir     : ${workflow.workDir}
    """
}

// Utility functions
def toHumanString(bytes) {
    // Thanks Niklaus
    // https://gist.github.com/nikbucher/9687112
    base = 1024L
    decimals = 3
    prefix = ['', 'K', 'M', 'G', 'T']
    int i = Math.log(bytes)/Math.log(base) as Integer
    i = (i >= prefix.size() ? prefix.size()-1 : i)
    return Math.round((bytes / base**i) * 10**decimals) / 10**decimals + prefix[i]
}

def print_version() {
    println(PROGRAM_NAME + ' ' + VERSION)
    exit 0
}

def print_usage() {
    log.info"""
    ${PROGRAM_NAME} ${VERSION}

    Required Parameters:
        --samples FILE          File produced by titan-gc-prepare.py (--tsv used)

        --run_name STR          Name to use for the results folder

    Optional Parameters:
        --titan_opts FILE       A JSON file with Titan GC parameters to use.
                                    Default: Use Titan-GC's defaults

        --cromwell_config FILE  A cromwell configuration to use.
                                    Default: Use conig associated with profile

        --profile STR           Profile for Titan GC to use
                                    Options: docker or singularity
                                    Default: ${params.profile}

        --singularity_cache STR 
                                Location where Singularity images are saved to
                                    Default: ${params.singularity_cache}

        --outdir DIR            Directory to write results to
                                    Default: ${params.outdir}

        --max_retry INT         Maximum number of times to retry a failed job
                                    Default: ${params.max_retry}

        --keep_files            Prevent removal of the "work" directory after 
                                    a successful run.
    """.stripIndent()
    exit 0
}

def file_exists(file_name, parameter) {
    if (!file(file_name).exists()) {
        log.error('Invalid input ('+ parameter +'), please verify "' + file_name + '" exists.')
        return 1
    }
    return 0
}

def check_input_params() {
    error = 0

    if (!params.samples || !params.run_name) {
        log.error """
        One or more required parameters are missing, please check and try again.
        Required Parameters:
            --samples FILE          File produced by titan-gc-prepare.py (--tsv used)

            --run_name STR          Name to use for the results folder
        """.stripIndent()
        error += 1
    }

    error += file_exists(params.samples, '--samples')

    if (params.titan_opts) {
        error += file_exists(params.titan_opts, '--titan_opts')
    }

    if (params.cromwell_config) {
        error += file_exists(params.cromwell_config, '--cromwell_config')
        CROMWELL_CONFIG = params.cromwell_config
    } else {
        CROMWELL_CONFIG = "${baseDir}/conf/cromwell-${params.profile}.config"
    }

    // Check for existing output directory
    if (!workflow.resume) {
        if (!file(params.outdir).exists() && !params.force) {
            log.error("Output directory (${params.outdir}) exists, will not continue unless '--force' is used.")
            error += 1
        }
    }

    if (!['docker', 'singularity'].contains(params.profile)) {
        log.error "Invalid profile (--profile ${params.profile}), must be 'docker', " +
                  "or 'singularity'. Please correct to continue."
        error += 1
    }

    if (error > 0) {
        log.error('Cannot continue, please see --help for more information')
        exit 1
    }

    return 1
}

def is_positive_integer(value, name) {
    error = 0
    if (value.getClass() == Integer || value.getClass() == BigDecimal) {
        if (value < 0) {
            log.error('Invalid input (--'+ name +'), "' + value + '"" is not a positive.')
            error = 1
        }
    } else {
        if (!value.isInteger()) {
            log.error('Invalid input (--'+ name +'), "' + value + '"" is not numeric.')
            error = 1
        } else if (value.toInteger() < 0) {
            log.error('Invalid input (--'+ name +'), "' + value + '"" is not a positive integer.')
            error = 1
        }
    }
    return error
}

def process_fofn(line) {
    /* Parse line and determine if single end or paired reads*/
    error = 0
    error += file_exists(line.r1, "${line.sample} 'r1' column")
    error += file_exists(line.r2, "${line.sample} 'r2' column")
    error += file_exists(line.primers, "${line.sample} 'primers' column")
    if (params.titan_opts) {
        error += file_exists(params.titan_opts, "--titan_opts")
    }

    if (error > 0) {
        log.error("Please check file locations in ${params.samples}")
        exit 1
    } else {
        if (params.titan_opts) {
            if (line.titan_wf) {
                return tuple(line.sample, line.titan_wf, [line.r1], [line.r2], line.primers, params.titan_opts, CROMWELL_CONFIG)
            } else {
                return tuple(line.sample, line.theiacov_wf, [line.r1], [line.r2], line.primers, params.titan_opts, CROMWELL_CONFIG)
            }
        } else {
            if (line.titan_wf) {
                return tuple(line.sample, line.titan_wf, [line.r1], [line.r2], line.primers, EMPTY_JSON, CROMWELL_CONFIG)
            } else {
                return tuple(line.sample, line.theiacov_wf, [line.r1], [line.r2], line.primers, EMPTY_JSON, CROMWELL_CONFIG)
            }
        }
    }
}

def create_input_channel() {
    return Channel.fromPath( params.samples )
            .splitCsv(header: true, sep: '\t')
            .map { row -> process_fofn(row) }
}
