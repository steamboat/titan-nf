#! /bin/bash
TITAN_NF="${PREFIX}/share/${PKG_NAME}-${PKG_VERSION}"
mkdir -p $PREFIX/bin ${TITAN_NF}

# Copy scripts
chmod 755 bin/*
cp -f bin/titan-nf $PREFIX/bin/
cp -f bin/gisaid-batch.py $PREFIX/bin/gisaid-batch
cp -f bin/verify-titan-outputs.py $PREFIX/bin/verify-titan-outputs.py

# Copy Nextflow workflow
cp -r ./ ${TITAN_NF}/
rm -rf ${TITAN_NF}/conda
rm -rf ${TITAN_NF}/tests
rm -rf ${TITAN_NF}/bin/titan-nf
rm -rf ${TITAN_NF}/bin/gisaid-batch.py
rm -rf ${TITAN_NF}/bin/verify-titan-outputs.py

# Setup the titan-nf env variables
mkdir -p ${PREFIX}/etc/conda/activate.d ${PREFIX}/etc/conda/deactivate.d
echo "export WPHL_CONSTANTS=${TITAN_NF}/data/wphl.yml" > ${PREFIX}/etc/conda/activate.d/titan-nf.sh
echo "export WSVL_CONSTANTS=${TITAN_NF}/data/wsvl.yml" >> ${PREFIX}/etc/conda/activate.d/titan-nf.sh
echo "export TITAN_NF_VERSION=${PKG_VERSION}" >> ${PREFIX}/etc/conda/activate.d/titan-nf.sh
echo "export TITAN_NF=${TITAN_NF}" >> ${PREFIX}/etc/conda/activate.d/titan-nf.sh
chmod a+x ${PREFIX}/etc/conda/activate.d/titan-nf.sh

# Unset them
echo "unset WPHL_CONSTANTS" > ${PREFIX}/etc/conda/deactivate.d/titan-nf.sh
echo "unset WSVL_CONSTANTS" > ${PREFIX}/etc/conda/deactivate.d/titan-nf.sh
echo "unset TITAN_NF_VERSION" >> ${PREFIX}/etc/conda/deactivate.d/titan-nf.sh
echo "unset TITAN_NF" >> ${PREFIX}/etc/conda/deactivate.d/titan-nf.sh
chmod a+x ${PREFIX}/etc/conda/deactivate.d/titan-nf.sh
