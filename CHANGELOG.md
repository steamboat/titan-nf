## v1.4.2 steamboat/titan-nf "Names Hill" - 2022/06/17

### `fixed`
- column headers for excluded samples

## v1.4.1 steamboat/titan-nf "Devil's Gate" - 2022/06/01

### `fixed`
- missing keys in `gisaid-batch`

## v1.4.0 steamboat/titan-nf "Granger Stage Station" - 2022/05/26

### `added`
- updated to theiacov-gc 2.1.2

### `fixed`
- new Nextflow versions and DSL1

## v1.3.0 steamboat/titan-nf "Ames Monument" - 2022/03/28

### `added`
- flag sampels that may have incorrect collection dates

## v1.2.6 steamboat/titan-nf "Sinks Canyon" - 2022/02/16

### `added`
- add default `covv_sampling_strategy` to config

### `fixed`
- fixed consensus file matching

## v1.2.5 steamboat/titan-nf "Seminoe" - 2022/01/26

### `added`
- support for fasta files
- new GISAID field `covv_sampling_strategy`

## v1.2.4 steamboat/titan-nf "Medicine Lodge" - 2021/12/16

### `added`
- ages 90 or older are output as '>90'

### `fixed`
- iseq not recognized

## v1.2.3 steamboat/titan-nf "Keyhole" - 2021/12/14

### `added`
- iseq as a instrument option
- updated wphl author list

### `fixed`
- failed runs when no samples were excluded
- check for compressed json files
- default `--max_retry` to 2

## v1.2.2 steamboat/titan-nf "Hot Springs" - 2021/11/02

### `added`
- exclude low coverage samples from analysis

### `fixed`
- typo in workflow name 

## v1.2.1 steamboat/titan-nf "Hawk Springs" - 2021/09/29

### `added`
- coverage reduction for Illumina (`reformat.sh`) and ONT (`rasusa`)
- `--coverage` defaults 30000x
- added cleanup steps after `titan-gc` run
- increased max runtime per process

### `fixed`
- assembly recognition when rerunning pangolin


## v1.2.0 steamboat/titan-nf "Guernsey" - 2021/09/15

### `added`
- WSVL constants will be grabbed with 'wsvl' value
- Added command to transfer to GCP
- Use `titan-gc` from bioconda (>=1.5.0)

### `removed`
- auto docker update

## v1.1.0 steamboat/titan-nf "Glendo" - 2021/08/23

### `added`
- WSVL constants will be grabbed with 'wsvl' value
- Added command to transfer to GCP
- Use `titan-gc` from bioconda (>=1.5.0)

### `fixed`
- files in FOFN are checked to exist
- Nextflow trimming the leading 0 from run_names

## v1.0.4 steamboat/titan-nf "Edness K. Wilkins" - 2021/08/10

### `added`
- pyyaml and biopython to conda recipe
- Use Gitlab CI to test the commands

### `fixed`
- a few bugs in gisaid-batch

## v1.0.3 steamboat/titan-nf "Curt Gowdy" - 2021/08/09

### `added`
- script to verify all outputs are available
  - `work` directory is only deleted if all
- `--cromwell_config` to provide a cromwell config
  - Defaults to `${baseDir}/conf/cromwell-${params.profile}.config`

### `Fixed`
- deletion of work directory before Nextflow finished copying files
- Disabled cache for `singularity build`

## v1.0.2 steamboat/titan-nf "Buffalo Bill" - 2021/08/06

### `Fixed`
- run failed if gisaid-batch was skipped

### `removed`
- automatic gisaid-batching

## v1.0.1 steamboat/titan-nf "Boysen" - 2021/08/05

### `Added`
- version info from ENV variable
- WPHL constants will be grabbed with 'wphl' value

### `Fixed`
- version info in gisaid-batch
- `--help` and `--version` info now come from `titan-nf` instead of Nextflow

## v1.0.0 steamboat/titan-nf "Bear River" - 2021/08/05
- Initial release of steamboat/titan-nf
