#! /usr/bin/env python3
import json
import sys

def read_versions(versions):
    with open(versions, 'rt') as version_fh:
        return version_fh.readline().rstrip()

def read_json(json_file):
    with open(json_file, 'rt') as json_fh:
        return json.loads(json_fh.readline().rstrip())

def read_pangolin(pangolin_results):
    keys = None
    with open(pangolin_results, 'rt') as pangolin_fh:
        for line in pangolin_fh:
            vals = line.rstrip().split(',')
            if keys:
                return dict(zip(keys, vals))
            else:
                keys = vals

if __name__ == '__main__':
    import argparse as ap

    parser = ap.ArgumentParser(
        prog='update-titan-results.py',
        conflict_handler='resolve',
        description=(f'Updates titan results if new Pangolin lineage is called')
    )

    parser.add_argument('outdir', metavar="STR", type=str, help='Directory to save outputs to')
    parser.add_argument('json', metavar="STR", type=str, help='Titan results is JSON format')
    parser.add_argument('pangolin', metavar="STR", type=str, help='Pangolin output to parse')
    parser.add_argument('versions', metavar="STR", type=str, help='Text file of Panglin versions')

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)

    args, unknown = parser.parse_known_args()
    titan_results = read_json(args.json)
    pango_results = read_pangolin(args.pangolin)
    pango_versions = read_versions(args.versions)

    final_results = {}
    for k,v in titan_results.items():
        final_results[k] = v
        if k == "pangolin_lineage":
            final_results['pangolin_changed'] = False
        elif k == "pangolin_versions":
            final_results['pangolin_history'] = ''

    # Compare Pangolin results
    if final_results['pangolin_lineage'] != pango_results['lineage']:
        # Mismatch, use most recent
        history_note = (
            f"{final_results['pangolin_lineage']} changed to {pango_results['lineage']} [pangolin_conflicts={final_results['pangolin_conflicts']},"
            f"pangolin_notes={final_results['pangolin_notes']},pangolin_assignment_version={final_results['pangolin_assignment_version']},"
            f"pangolin_docker={final_results['pangolin_docker']},pangolin_versions{final_results['pangolin_versions']}]"
        )
        final_results['pangolin_lineage'] = pango_results['lineage']
        final_results['pangolin_changed'] = True
        final_results['pangolin_conflicts'] = pango_results['conflict']
        final_results['pangolin_notes'] = pango_results['note']
        final_results['pangolin_assignment_version'] = f"pangolin {pango_results['pangolin_version']};{pango_results['version']}"
        final_results['pangolin_docker'] = 'bioconda'
        final_results['pangolin_versions'] = pango_versions
        final_results['pangolin_history'] = history_note

    # Write new outputs
    json_out = f'{args.outdir}/{final_results["sample"]}.json'
    tsv_out = f'{args.outdir}/{final_results["sample"]}.json'
    with open(f'{args.outdir}/{final_results["sample"]}.json', 'wt') as json_fh:
        json.dump(final_results, json_fh)

    with open(f'{args.outdir}/{final_results["sample"]}.tsv', 'wt') as tsv_fh:
        cols = final_results.keys()
        tsv_header = '\t'.join(cols)
        tsv_fh.write(f'{tsv_header}\n')
        vals = []
        for col in cols:
            vals.append(str(final_results[col]))
        tsv_vals = '\t'.join(vals)
        tsv_fh.write(f'{tsv_vals}\n')
