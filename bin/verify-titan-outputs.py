#! /usr/bin/env python3
"""


"""
import os
import sys
TITAN = __import__("theiacov-gc-organize")
OUTPUTS = TITAN.OUTPUTS

def file_exists(filename, debug=False):
    if os.path.exists(filename):
        if debug:
            print(f'DEBUG: Found {filename}', file=sys.stderr)
        return 0
    print(f'ERROR: Unable to locate {filename}', file=sys.stderr)
    return 1

if __name__ == '__main__':
    import argparse as ap
    from pathlib import Path
    import json

    parser = ap.ArgumentParser(
        prog='verify-titan-outpts',
        conflict_handler='resolve',
        description=(
            f'titan-gc-make-inputs - Creates a input JSON for a single sample'
        )
    )

    parser.add_argument('--samples', metavar="STR", type=str, help='File produced by titan-gc-prepare.py (--tsv used)')
    parser.add_argument('--run_name', metavar="STR", type=str, help='Name to use for the results folder')
    parser.add_argument('--outdir', metavar="STR", type=str, help='Directory to write results to', default="./")
    parser.add_argument('--debug', action='store_true', help='Print helpful information')

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)

    args, unknown = parser.parse_known_args()


    run_directory = f'{args.outdir}/{args.run_name}'.replace('//', '/')
    if args.run_name.startswith("0"):
        alternate_run_directory = f'{args.outdir}/{args.run_name.lstrip("0")}'.replace('//', '/')
        if os.path.exists(alternate_run_directory):
            run_directory = alternate_run_directory
    samples = []
    with open(args.samples, 'rt') as sample_fh:
        header = True
        for line in sample_fh:
            if not header:
                # first column is the sample name
                samples.append(line.rstrip().split('\t')[0])
            else:
                header = False

    total_errors = 0
    for sample in samples:
        if os.path.exists(f'{run_directory}/excluded/{sample}.json'):
            if args.debug:
                print(f'DEBUG: {sample} excluded from analysis, skipping checks', file=sys.stderr)
            continue
        metadata_file = f'{run_directory}/cromwell/{sample}-metadata.json'
        if file_exists(metadata_file, args.debug) == 0:
            metadata = None
            with open(metadata_file, 'rt') as metadata_fh:
                metadata = json.load(metadata_fh)

            for key, outputs in metadata["outputs"].items():
                taskname = key.replace("titan_gc.", "")
                if taskname not in ['summaries_json', 'summaries_tsv']:
                    if type(outputs) is list:
                        for output in outputs:
                            filename = os.path.basename(output)
                            result = OUTPUTS[taskname]['folder']
                            filepath = f'{run_directory}/{result}/{filename}'
                            if taskname == "kraken_report_dehosted":
                                filepath = filepath.replace('_kraken2_report.txt', '_dehosted_kraken2_report.txt')
                            elif taskname == "auspice_json" or taskname == "nextclade_json":
                                filepath = filepath.replace('.json', '.json.gz')
                            elif filepath.endswith('.results.json'):
                                filepath = filepath.replace('.results.json', '.json')
                            total_errors += file_exists(filepath, args.debug)
                    else:
                        filename = os.path.basename(outputs)
                        result = OUTPUTS[taskname]['folder']
                        filepath = f'{run_directory}/{result}/{filename}'
                        total_errors += file_exists(filepath, args.debug)
        else:
            total_errors += 1

    if args.debug or total_errors:
        if total_errors:
            print(f"ERROR: {total_errors} files could not be located.", file=sys.stderr)
        else:
            print("DEBUG: All files accounted for", file=sys.stderr)
    sys.exit(total_errors)
