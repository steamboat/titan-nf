#! /bin/bash -uex
# Transfer completed runs to Google Storage Bucket
RUN_DIR="/opt/titan/runs"
DATA_DIR="${RUN_DIR}/data"
TARBALL_DIR="${RUN_DIR}/completed"
DATE=$(date)
GCP="gs://wphlngs/sars-cov-2/"
for f in ${RUN_DIR}/*.tar.gz; do
    if [[ -f "${f}" ]]; then
        LAB=$(basename "${f}" | sed -E 's/(wsvl|wphl).*/\1/;')
        RUN_NAME=$(basename "${f}" | sed 's/wsvl-\|wphl-//;s/.tar.gz//')
        TRANSFER_DIR="${DATA_DIR}/${LAB}"
        mkdir -p ${TRANSFER_DIR}
        mkdir -p ${TARBALL_DIR}

        # Extract tarball
        tar -xzf ${f} -C ${TRANSFER_DIR}

        # Copy to GCPS
        gsutil -m copy -r ${DATA_DIR} ${GCP}

        # mv tarball
        mv ${f} ${TARBALL_DIR}

        # Add details
        echo "${DATE},${RUN_NAME},${GCP}/${LAB}/${RUN_NAME}" >> ${RUN_DIR}/completed-runs.txt
        cut -f 1 ${TRANSFER_DIR}/${RUN_NAME}/titan-results.tsv | grep -v sample | xargs -I {} echo "{},${RUN_NAME}" >> completed-samples.txt

        # Cleanup
        rm -rf ${DATA_DIR}
    fi
done
