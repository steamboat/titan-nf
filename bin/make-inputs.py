#! /usr/bin/env python3
"""
usage: titan-gc-make-inputs [-h] [--sample STR] [--workflow STR] [--r1 STR] [--r2 STR] [--primers PRIMER] 
                            [--pangolin_docker STR] [--params STR]

titan-gc-make-inputs - Creates a input JSON for a single sample

optional arguments:
  -h, --help            show this help message and exit

Titan-GC Prepare Parameters:
  --sample STR          Directory where FASTQ files are stored
  --workflow STR        The Titan-GC workflow to use for analysis. Options: clearlabs, fasta, illumina_pe, illumina_se, ont
  --r1 STR              R1 FASTQ of a read pair or single-end FASTQ.
  --r2 STR              R2 FASTQ of a read pair
  --primers PRIMER      A file containing primers (bed format) used during sequencing.

Optional Titan-GC Workflow Parameters:
  --pangolin_docker STR
                        Docker image used to run Pangolin (takes priority over --params)
  --params STR      Seqeuncing method used
"""

if __name__ == '__main__':
    import argparse as ap
    from pathlib import Path
    import json
    import os
    import sys

    parser = ap.ArgumentParser(
        prog='titan-gc-make-inputs',
        conflict_handler='resolve',
        description=(
            f'titan-gc-make-inputs - Creates a input JSON for a single sample'
        )
    )

    group1 = parser.add_argument_group('Titan-GC Prepare Parameters')
    group1.add_argument(
        '--sample', metavar="STR", type=str, help='Directory where FASTQ files are stored'
    )
    group1.add_argument(
        '--workflow', metavar='STR', type=str, choices=['clearlabs', 'fasta', 'illumina_pe', 'illumina_se', 'ont'],
        help='The Titan-GC workflow to use for analysis. Options: clearlabs, fasta, illumina_pe, illumina_se, ont'
    )
    group1.add_argument(
        '--r1', metavar='STR', type=str,
        help='R1 FASTQ of a read pair or single-end FASTQ.'
    )
    group1.add_argument(
        '--r2', metavar='STR', type=str,
        help='R2 FASTQ of a read pair'
    )
    group1.add_argument(
        '--primers', metavar='PRIMER', type=str,
        help='A file containing primers (bed format) used during sequencing.'
    )

    group2 = parser.add_argument_group('Optional Titan-GC Workflow Parameters')
    group2.add_argument(
        '--pangolin_docker', metavar='STR', type=str,
        help='Docker image used to run Pangolin (takes priority over --params)'
    )
    group2.add_argument(
        '--params', metavar='STR', type=str, help='A JSON file containing parameter values'
    )

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)

    args = parser.parse_args()
    EMPTY_FASTQ = f"{str(Path.home())}/.titan/EMPTY.fastq.gz"
    if not os.path.exists(EMPTY_FASTQ):
        Path(f"{str(Path.home())}/.titan").mkdir(parents=True, exist_ok=True)
        with open(EMPTY_FASTQ, 'a'):
            pass

    inputs_json = {
        "theiacov_gc.samples": [
            {
                'sample': args.sample, 
                'theiacov_wf': args.workflow,
                'r1': str(Path(args.r1).absolute()),
                'r2': str(Path(args.r2).absolute()) if args.r2 else EMPTY_FASTQ,
                'primers': str(Path(args.primers).absolute())
            }
        ]
    }
    params_json = {}

    # Add optional parameters if user specified them
    if args.params:
        with open(args.params, 'rt') as json_fh:
            params_json = json.load(json_fh)

    print(json.dumps({**inputs_json, **params_json}, indent = 4))
