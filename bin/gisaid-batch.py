#! /usr/bin/env python3
"""
usage: gisaid-batch [-h] [--prefix STR] [--min_coverage FLOAT] [--max_n INT]
                    [--extension STR] [--outdir STR] [--force] [--verbose] [--silent] [--version]
                    THEIACOV_RESULTS ASSEMBLIES METADATA SEQUENCER CONSTANT_YAML

gisaid-batch - Format data for GISAID submission

positional arguments:
  THEIACOV_RESULTS      Output from TheiaCoV to apply filtering (e.g. percent coverage, number of Ns).
  ASSEMBLIES            Directory of FASTA assemblies to be uploaded
  METADATA              Text file associated with the Run.
  SEQUENCER             Sequencer used to generate sequences. (clearlabs, hiseq, miseq, nextseq, ont)
  CONSTANT_YAML         A YAML formatted file containing constant information for GISAID fields
                            If 'wphl' or 'wsvl' is given it will find the YAML associated with wphl

optional arguments:
  -h, --help            show this help message and exit
  --prefix STR          Prefix for output files. (Default: Use the FASTA directory name)
  --min_coverage FLOAT  Minimum assembly coverage to submit to GISAID. (Default: 70)
  --max_n INT           Maximum number of Ns allowed to submit to GISAID. (Default: 7500)
  --extension STR       Extension of the of the input FASTAs. (Default: .fasta)
  --outdir STR          Directory to output files. (Default: ./)
  --force               Overwrite existing files. (Default: do not overwrite)
  --verbose             Print debug related text.
  --silent              Only critical errors will be printed.
  --version             show program's version number and exit

example usage:
gisaid-batch THEIACOV_RESULTS ASSEMBLIES METADATA SEQUENCER CONSTANT_YAML
"""
import os
import sys
from collections import OrderedDict
PROGRAM = "gisaid-batch"
VERSION = os.environ.get('TITAN_NF_VERSION')
STDOUT = 11
STDERR = 12

# GISAID Related (last updated 6/11/2021)
GISAID_TYPE = 'betacoronavirus'
GISAID_FIELDS = [
    'submitter',
    'fn',
    'covv_virus_name',
    'covv_type',
    'covv_passage',
    'covv_collection_date',
    'covv_location',
    'covv_add_location',
    'covv_host',
    'covv_add_host_info',
    'covv_sampling_strategy',
    'covv_gender',
    'covv_patient_age',
    'covv_patient_status',
    'covv_specimen',
    'covv_outbreak',
    'covv_last_vaccinated',
    'covv_treatment',
    'covv_seq_technology',
    'covv_assembly_method',
    'covv_coverage',
    'covv_orig_lab',
    'covv_orig_lab_addr',
    'covv_provider_sample_id',
    'covv_subm_lab',
    'covv_subm_lab_addr',
    'covv_subm_sample_id',
    'covv_consortium',
    'covv_authors',
    'covv_comment',
    'comment_type'
]

GISAID_HEADERS = {
    'submitter': 'Submitter',
    'fn': 'FASTA filename',
    'covv_virus_name': 'Virus name',
    'covv_type': 'Type',
    'covv_passage': 'Passage details/history',
    'covv_collection_date': 'Collection date',
    'covv_location': 'Location',
    'covv_add_location': 'Additional location information',
    'covv_host': 'Host',
    'covv_add_host_info': 'Additional host information',
    'covv_sampling_strategy': 'Sampling Strategy',
    'covv_gender': 'Gender',
    'covv_patient_age': 'Patient age',
    'covv_patient_status': 'Patient status',
    'covv_specimen': 'Specimen source',
    'covv_outbreak': 'Outbreak',
    'covv_last_vaccinated': 'Last vaccinated',
    'covv_treatment': 'Treatment',
    'covv_seq_technology': 'Sequencing technology',
    'covv_assembly_method': 'Assembly method',
    'covv_coverage': 'Coverage',
    'covv_orig_lab': 'Originating lab',
    'covv_orig_lab_addr': 'Address',
    'covv_provider_sample_id': 'Sample ID given by the sample provider',
    'covv_subm_lab': 'Submitting lab',
    'covv_subm_lab_addr': 'Address',
    'covv_subm_sample_id': 'Sample ID given by the submitting laboratory',
    'covv_consortium': 'Sequencing consortium',
    'covv_authors': 'Authors',
    'covv_comment': 'Comment',
    'comment_type': 'Comment Icon'
}

def gisaid_formatter(metadata, gisaid_fasta, seq_technology, constant_yaml, source=''):
    """
    Takes in metadata associated with a sample and returns the GISAID formatted data.
    """
    import yaml
    constants = None
    with open(constant_yaml, 'r') as yaml_fh:
        constants = yaml.safe_load(yaml_fh)
    authors = "{}, and {}".format(", ".join(constants['AUTHORS'][:-1]), constants['AUTHORS'][-1])
    year = metadata['collection_date'].split("-")[0]
    virus_name = f'hCoV-19/{constants["COUNTRY"]}/{constants["SUBMISSION_ID_PREFIX"]}{metadata["state_key"]}/{year}'
    if metadata['age']:
        if not metadata['age'].isdigit():
            logging.error(f'Found "{metadata["age"]}" for age. Ages must be an integer, please check your metadata file.')
            sys.exit(1)
    GISAID_LOCATION = f'{constants["CONTINENT"]} / {constants["COUNTRY"]} / {constants["STATE"]}'
    return OrderedDict((
        ('submitter', constants['SUBMITTER']),
        ('fn', gisaid_fasta),
        ('covv_virus_name', virus_name),
        ('covv_type', GISAID_TYPE),
        ('covv_passage', constants['GISAID_PASSAGE']),
        ('covv_collection_date', metadata['collection_date']),
        ('covv_location', GISAID_LOCATION),
        ('covv_add_location', ''),
        ('covv_host', constants['GISAID_HOST']),
        ('covv_add_host_info', ''),
        ('covv_sampling_strategy', constants['GISAID_SAMPLING_STRATEGY'] if 'GISAID_SAMPLING_STRATEGY' in constants else ''),
        ('covv_gender', metadata['sex'].lower()),
        ('covv_patient_age', '>90' if int(metadata['age']) >= 90 else metadata['age']),
        ('covv_patient_status', constants['GISAID_PATIENT_STATUS']),
        ('covv_specimen', source),
        ('covv_outbreak', ''),
        ('covv_last_vaccinated', ''),
        ('covv_treatment', ''),
        ('covv_seq_technology', seq_technology),
        ('covv_assembly_method', ''),
        ('covv_coverage', ''),
        ('covv_orig_lab', constants['ORIGINATING_LAB']),
        ('covv_orig_lab_addr', constants['ORIGINATING_LAB_ADDRESS']),
        ('covv_provider_sample_id', ''),
        ('covv_subm_lab', constants['SUBMITTING_LAB']),
        ('covv_subm_lab_addr', constants['SUBMITTING_LAB_ADDRESS']),
        ('covv_subm_sample_id', ''),
        ('covv_consortium', ''),
        ('covv_authors', authors),
        ('covv_comment', ''),
        ('comment_type', '')
    ))

def check_inputs(inputs):
    """Verify inputs exist."""
    has_error = False
    for input in inputs:
        if not os.path.exists(input):
            print(f'Unable to open {input}, please verify the path.', file=sys.stderr)
            has_error = True
    if has_error:
        sys.exit(1)

def read_tsv(tsv, has_header=True, delimiter='\t'):
    """
    Read a TSV and return a list of dicts or list of lists depending on has_header

    has_header = True # return a list of dicts (e.g. [{'sample':'xyz', 'run':'1'}, {'sample':'abc', 'run':'1'}])
    has_header = False # return a list of lists (e.g. [['xyz','1'], ['abc','1']])
    """
    col_names = None
    rows = []
    with open(tsv, 'rt') as tsv_fh:
        for line in tsv_fh:
            cols = line.rstrip('\n').split(delimiter)
            if has_header and not col_names:
                col_names = cols
            else:
                if has_header:
                    rows.append(dict(zip(col_names, cols)))
                else:
                    rows.append(cols)
    return rows

def read_csv(csv, has_header=True):
    """
    Reads a CSV and returns a list of dicts or list of lists depending on has_header
    """
    return read_tsv(csv, has_header=has_header, delimiter=",")

def read_fasta(fasta, as_dict=True):
    """Parse the input FASTA file."""
    from Bio import SeqIO
    seqs = {} if as_dict else []
    with open(fasta, 'r') as fasta_fh:
        for record in SeqIO.parse(fasta_fh,'fasta'):
            if as_dict:
                seqs[record.name] = str(record.seq)
            else:
                seqs.append(record.seq)
    return seqs

def read_consensus_fasta(fasta, header=''):
    """Parse the input consensus FASTA file. There should be only one sequence."""
    seq = read_fasta(fasta, as_dict=False) if header else read_fasta(fasta)
    if len(seq) == 1:
        if header:
            return {'record': header, 'sequence': seq[0]}
        else:
            for k,v in seq.items:
                return {'record': k, 'sequence': v}
    elif len(seq) > 1:
        logging.warning(f'More than one sequence ({len(seq)}) found in {fasta}, expected 1.')
    else:
        logging.warning(f'No sequences found in {fasta}')
    return None

def main():
    import argparse as ap
    import glob
    import textwrap
    import logging
    import time
    from pathlib import Path

    logging.addLevelName(STDOUT, "STDOUT")
    logging.addLevelName(STDERR, "STDERR")

    parser = ap.ArgumentParser(
        prog=PROGRAM,
        conflict_handler='resolve',
        description=f'{PROGRAM} (v{VERSION}) - Format data for GISAID submission',
        formatter_class=ap.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent(f'''
            example usage:
            {PROGRAM} THEIACOV_RESULTS ASSEMBLIES METADATA SEQUENCER CONSTANT_YAML
        ''')
    )
    parser.add_argument('titan', metavar="THEIACOV_RESULTS", type=str,
                        help='Output from Titan to apply filtering (e.g. percent coverage, number of Ns).')
    parser.add_argument('fasta_dir', metavar="ASSEMBLIES", type=str, 
                        help='Directory of FASTA assemblies to be uploaded')
    parser.add_argument('metadata', metavar="METADATA", type=str, help='Text file associated with the Run.')
    parser.add_argument('sequencer', metavar="SEQUENCER", type=str, choices=['clearlabs', 'iseq', 'hiseq', 'miseq', 'nextseq', 'ont'],
                        help='Sequencer used to generate sequences. (clearlabs, hiseq, miseq, nextseq, ont)')
    parser.add_argument('constants', metavar="CONSTANT_YAML", type=str,
                        help='A YAML formatted file containing constant information for GISAID fields. If "wphl" is given it will find the YAML associated with wphl')
    parser.add_argument('--prefix', metavar='STR', type=str,
                        help='Prefix for output files. (Default: Use the FASTA directory name)')
    parser.add_argument('--min_coverage', metavar='FLOAT', type=float, default=70, 
                        help='Minimum assembly coverage to submit to GISAID. (Default: 70)')
    parser.add_argument('--max_n', metavar='INT', type=int, default=7500, 
                        help='Maximum number of Ns allowed to submit to GISAID. (Default: 7500)')
    parser.add_argument('--extension', metavar='STR', type=str, default='.fasta',
                        help='Extension of the of the input FASTAs. (Default: .fasta)')
    parser.add_argument('--outdir', metavar='STR', type=str, default="./",
                        help='Directory to output files. (Default: ./)')
    parser.add_argument('--force', action='store_true', help='Overwrite existing files. (Default: do not overwrite)')
    parser.add_argument('--verbose', action='store_true', help='Print debug related text.')
    parser.add_argument('--silent', action='store_true', help='Only critical errors will be printed.')
    parser.add_argument('--version', action='version', version=f'{PROGRAM} {VERSION}')

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)

    args = parser.parse_args()

    #Check if inputs exist
    constants_file = args.constants
    sampling_strategy = ''
    if args.constants == 'wphl':
        constants_file = os.environ.get('WPHL_CONSTANTS')
        sampling_strategy = "Baseline surveillance"
    elif args.constants == 'wsvl':
        constants_file = os.environ.get('WSVL_CONSTANTS')
    check_inputs([args.titan, args.fasta_dir, args.metadata, constants_file])

    # Assembly method
    SEQUENCER = {
        'clearlabs': 'Oxford Nanopore Technologies (via ClearLabs)',
        'iseq': 'Illumina iSeq',
        'hiseq': 'Illumina HiSeq',
        'miseq': 'Illumina MiSeq',
        'nextseq': 'Illumina NextSeq',
        'ont': 'Oxford Nanopore Technologies'
    }
    seq_technology = SEQUENCER[args.sequencer]

    # Setup logs
    FORMAT = '%(asctime)s:%(name)s:%(levelname)s - %(message)s'
    logging.basicConfig(format=FORMAT, datefmt='%Y-%m-%d %H:%M:%S',)
    logging.getLogger().setLevel(logging.ERROR if args.silent else logging.DEBUG if args.verbose else logging.INFO)

    # Make sure files don't exist
    fasta_dir = str(Path(args.fasta_dir).resolve())
    outdir = str(Path(args.outdir).resolve())
    prefix = args.prefix if args.prefix else os.path.basename(fasta_dir)
    gisaid_fasta = f'{outdir}/{prefix}-gisaid.fasta'
    gisaid_fasta_name = f'{prefix}-gisaid.fasta'
    gisaid_metadata = f'{outdir}/{prefix}-gisaid.csv'
    gisaid_excluded = f'{outdir}/{prefix}-gisaid.excluded.txt'
    exists = False
    for output in [gisaid_fasta, gisaid_metadata, gisaid_excluded]:
        if os.path.exists(output) and not args.force:
            logging.error(f'{output} exists.')
            exists = True
    if exists:
        logging.error(f'Found existing files, will not continue unless `--force` is used.')
        sys.exit()

    # Read Metadata
    logging.info(f'Reading {args.metadata}')
    metadata = read_tsv(args.metadata, has_header=True)
    logging.debug(f'Found {len(metadata)} samples in {args.metadata}')

    # Read result files
    titan = {}
    for result in read_tsv(args.titan, has_header=True):
        titan[result['sample']] = result
    logging.debug(f'Found {len(titan)} samples in {args.titan}')

    if metadata:
        logging.info(f'Formatting for GISAID submission')
        excluded = []
        fasta_seqs = []
        gisaid_prepped = []
        result_data = []
        for sample in metadata:
            sample_id = sample['state_key']
            result_id = None
            excluded_reason = []

            for key in titan.keys():
                if sample_id in key:
                    logging.debug(f'Associated {sample_id} with TheiaCoV output: {key}')
                    result_id = key

            if result_id:
                if titan[result_id]['theiacov_workflow'] == 'excluded':
                    excluded_reason.append(titan[result_id]['theiacov_version'])
                else:
                    percent_cvg = float(titan[result_id]['percent_reference_coverage'])
                    number_n = int(titan[result_id]['number_n'])
                    pangolin_lineage = titan[result_id]['pangolin_lineage']
                    check_date = time.strptime(sample['collection_date'], "%Y-%m-%d") < time.strptime("2021-12-11", "%Y-%m-%d") if sample['collection_date'] else False
                    if check_date and pangolin_lineage.startswith("BA"):
                        excluded_reason.append(f'Collection date ({sample["collection_date"]}) of Omicron ({pangolin_lineage}) is older than expected (2021-12-11), please verify collection date')
                    elif percent_cvg >= args.min_coverage and number_n < args.max_n:
                        source = ""
                        if "source" in sample:
                            source = sample["source"]
                        gisaid_data = gisaid_formatter(sample, gisaid_fasta_name, seq_technology, constants_file, source=source)
                        consensus_fasta = glob.glob(f'{fasta_dir}/{result_id}*.fasta')
                        if len(consensus_fasta) == 1:
                            consensus_fasta = consensus_fasta[0]
                            logging.debug(f'Found {consensus_fasta}')
                            consensus = read_consensus_fasta(consensus_fasta, header=gisaid_data['covv_virus_name'])
                            if consensus:
                                fasta_seqs.append(consensus)
                                gisaid_prepped.append(gisaid_data)
                            else:
                                excluded_reason.append(f'Unexpected consensus assembly')
                        elif len(consensus_fasta) > 1:
                            logging.warning(f'Found multiple consensus FASTAs associated with {sample_id}: {consensus_fasta}')
                        else:
                            logging.warning(f'Unable to find a consensus FASTA associated with {sample_id}')
                    else:
                        if percent_cvg < args.min_coverage:
                            excluded_reason.append(f'Low percent coverage ({percent_cvg}%). Expected >= {args.min_coverage}%')
                        if number_n >= args.max_n:
                            excluded_reason.append(f'High number of Ns ({number_n}). Expected < {args.max_n}')

                result_data.append({
                    'sample_well': sample['sample_well'],
                    'state_key': sample['state_key'],
                    'percent_reference_coverage': titan[result_id]['percent_reference_coverage'],
                    'number_n': titan[result_id]['number_n'],
                    'vadr_num_alerts': titan[result_id]['vadr_num_alerts'],
                    'pangolin_lineage': titan[result_id]['pangolin_lineage'],
                    'nextclade_clade': titan[result_id]['nextclade_clade'],
                    'pangolin_docker': titan[result_id]['pangolin_docker'],
                    'pangolin_conflicts': titan[result_id]['pangolin_conflicts'],
                    'pangolin_notes': titan[result_id]['pangolin_notes'],
                    'nextclade_aa_subs': titan[result_id]['nextclade_aa_subs'],
                    'nextclade_aa_dels': titan[result_id]['nextclade_aa_dels'],
                    'comment': ";".join(excluded_reason)
                })
            else:
                result_data.append({
                    'sample_well': sample['sample_well'],
                    'state_key': sample['state_key'],
                    'percent_reference_coverage': '',
                    'number_n': '',
                    'vadr_num_alerts': '',
                    'pangolin_lineage': '',
                    'nextclade_clade': '',
                    'pangolin_docker': '',
                    'pangolin_conflicts': '',
                    'pangolin_notes': '',
                    'nextclade_aa_subs': '',
                    'nextclade_aa_dels': '',
                    'comment': f'Unable to associate {sample_id} with TheiaCoV results.'
                })
                logging.warning(f'Unable to associate {sample_id} with TheiaCoV results.')

            if excluded_reason:
                reasons = ";".join(excluded_reason)
                logging.info(f'Excluded {sample_id}, reason: {reasons}')
                excluded.append([sample_id, ";".join(excluded_reason)])

        # Write the outputs
        logging.info(f'Writing outputs')
        
        # GISAID Metadata
        with open(gisaid_metadata, 'wt') as metadata_fh:
            header1 = []
            header2 = []
            for field in GISAID_FIELDS:
                header1.append(f'"{field}"')
                header2.append(f'"{GISAID_HEADERS[field]}"')
            header1_description = ','.join(header1)
            header2_description = ','.join(header2)
            metadata_fh.write(f'{header1_description}\n')
            metadata_fh.write(f'{header2_description}\n')
            for sample in gisaid_prepped:
                row = []
                for field in GISAID_FIELDS:
                    row.append(f'"{sample[field]}"')
                row_data = ','.join(row)
                metadata_fh.write(f'{row_data}\n')
            logging.info(f'Wrote metadata for {len(gisaid_prepped)} samples to {gisaid_metadata}')

        # GISAID FASTA
        with open(gisaid_fasta, 'wt') as fasta_fh:
            for fasta in fasta_seqs:
                fasta_fh.write(f'>{fasta["record"]}\n')
                fasta_fh.write(f'{fasta["sequence"]}\n')
            logging.info(f'Wrote FASTA sequences for {len(fasta_seqs)} samples to {gisaid_fasta}')

        if excluded:
            # GISAID Excluded
            with open(gisaid_excluded, 'wt') as excluded_fh:
                excluded_fh.write(f'sample\treason\n')
                for sample, reason in excluded:
                    excluded_fh.write(f'{sample}\t{reason}\n')
                logging.info(f'Wrote reason for excluding {len(excluded)} samples to {gisaid_excluded}')

        # Titan
        gisaid_titan = f'{outdir}/{prefix}-titan.txt'
        with open(gisaid_titan, 'wt') as titan_fh:
            fields = ['sample_well', 'state_key', 'percent_reference_coverage', 'number_n', 'vadr_num_alerts', 
                      'pangolin_lineage', 'nextclade_clade', 'pangolin_docker', 'pangolin_conflicts', 'pangolin_notes', 
                      'nextclade_aa_subs', 'nextclade_aa_dels', 'comment']

            header = []
            print_header = True
            for result in result_data:
                cols = []
                for field in fields:
                    if field in result:
                        cols.append(result[field])
                        header.append(field)
                if print_header:
                    header_str = '\t'.join(header)
                    titan_fh.write(f'{header_str}\n')
                    print_header = False
                row = "\t".join(cols)
                titan_fh.write(f'{row}\n')
            logging.info(f'Wrote filtered Titan columns to {gisaid_titan}')
    else:
        logging.info(f'No metadata found to process, exiting...')

if __name__ == '__main__':
    main()
