#! /usr/bin/env python3
"""
usage: make-excluded-results [-h] [--sample STR] [--reason STR] [--outdir STR]

make-excluded-results - Creates an empty theiacov-results file

optional arguments:
  -h, --help            show this help message and exit

Parameters:
  --sample STR        Name of the sample
  --reason STR        The reason the sample was excluded from analysis
  --outdir STR        Directory to output results
"""

if __name__ == '__main__':
    import argparse as ap
    import json
    import sys

    parser = ap.ArgumentParser(
        prog='make-excluded-results',
        conflict_handler='resolve',
        description=(
            f'make-excluded-results - Creates an empty theiacov-results file'
        )
    )

    group1 = parser.add_argument_group('Parameters')
    group1.add_argument('--sample', metavar="STR", type=str, help='Name of the sample')
    group1.add_argument('--reason', metavar='STR', type=str, help='The reason the sample was excluded from analysis')
    group1.add_argument('--outdir', metavar='STR', type=str, help='Directory to output results', default="./")

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)

    args = parser.parse_args()
    THEIACOV_RESULTS = {
        "sample": f"{args.sample}",
        "theiacov_workflow": "excluded",
        "theiacov_version": f"{args.reason}",
        "theiacov_analysis_date": "",
    }

    with open(f'{args.outdir}/{args.sample}.json', 'wt') as json_fh:
        json.dump(THEIACOV_RESULTS, json_fh)

    with open(f'{args.outdir}/{args.sample}.tsv', 'wt') as tsv_fh:
        cols = THEIACOV_RESULTS.keys()
        col_string = '\t'.join(cols)
        tsv_fh.write(f'{col_string}\n')
        vals = []
        for col in cols:
            vals.append(THEIACOV_RESULTS[col])
        val_string = '\t'.join(vals)
        tsv_fh.write(f'{val_string}\n')
