# `titan-nf`, a Titan-GC Nextflow wrapper 
[Titan Genomic Characterization](https://github.com/theiagen/public_health_viral_genomics) (Titan-GC) is a workflow developed by 
Kevin Libuit for the analysis of viral pathogens, specifically SARS-CoV-2. Titan-GC is written in 
[Workflow Description Language (WDL)](https://openwdl.org/) with the [Terra](https://terra.bio/) cloud platform being 
its target environment (and it does the quite well!).

A command line interface (CLI) version of Titan-GC is available, which allows the user to run all Titan-GC workflows (*clearlabs, 
illumina_pe, illumina_se, and ont*) through a single command. This is done using [Cromwell](https://cromwell.readthedocs.io/en/stable/), 
via `cromwell run`, which does not support call-caching (e.g. jobs cannot be resumed, please if I'm wrong let me know!). There 
is `cromwell server` which supports call-caching, but its a big ask to process a single genome. I also explored 
[miniwdl](https://miniwdl.readthedocs.io/en/latest/), which allows resuming jobs, but does not currently support Singularity images.

Due to this I decided to make a Nextflow wrapper, `titan-nf`, around the Titan-GC CLI. This wrapper runs one genome per-process trough 
Titan-GC. In other words what this means is if one sample fails, the whole run doesn't fail, instead if one sample fails, Nextflow will 
just retry it and keep everything moving.

# _Disclaimer_
`titan-nf` is highly configured for our setup, so it might not work for you. If you would still like to use `titan-nf`, don't hesistate to reach out you think I might be able to help.

To get started using `titan-nf` head on over to the [Titan-NF Documentation](https://steamboat.gitlab.io/titan-nf/)
